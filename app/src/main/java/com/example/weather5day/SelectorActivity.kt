package com.example.weather5day

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import FiveDaysPrimary
import android.content.Intent
import android.view.Gravity
import com.example.weather5day.SpinnerModel.Country
import com.example.weather5day.SpinnerModel.CountryAdapter
import org.slf4j.LoggerFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.logging.Logger

class SelectorActivity : AppCompatActivity() {
    lateinit var countrycodes : Spinner
    lateinit var cityname : TextView
    lateinit var wether : FiveDaysPrimary
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selector)
        val counties = ArrayList<Country>()
        counties.add(Country("pl",R.drawable.poland))
        counties.add(Country("de",R.drawable.germany))
        counties.add(Country("fr",R.drawable.frabce))
        counties.add(Country("ru",R.drawable.russia))
        counties.add(Country("gb",R.drawable.britan))
        counties.add(Country("dk",R.drawable.danmark))
        counties.add(Country("fi",R.drawable.suomi))
        counties.add(Country("us",R.drawable.usa))
        counties.add(Country("il",R.drawable.israel))
        countrycodes = findViewById(R.id.Countrycodes)
        cityname = findViewById(R.id.Cityname)
        val rAdapter = CountryAdapter(this,counties)
        countrycodes.adapter = rAdapter
        val gobutton = findViewById(R.id.GoButton) as Button

        gobutton.setOnClickListener(){
            var selectedCT = countrycodes.selectedItem as Country
            val selectedintent = Intent(applicationContext,WeatherDisplayActivity::class.java)
            selectedintent.putExtra("countrycode",selectedCT.code)
            selectedintent.putExtra("citynam",cityname.text.toString())
            selectedintent.putExtra("isSelected",true)
            startActivity(selectedintent)
        }
    }
}
