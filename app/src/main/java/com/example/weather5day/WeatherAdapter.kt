package com.example.weather5day

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import list

class WeatherAdapter(val weatherlist: List<list>) : RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {
    lateinit var cntx : Context
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0?.context).inflate(R.layout.weather_item,p0,false)
        cntx = p0?.context
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return weatherlist.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val wt : list = weatherlist[p1]
        p0?.foredate.text = wt.dt_txt
        p0?.forecondi.text = wt.weather[0].main
        p0?.forepsi.text = wt.main.pressure.toString()+" PSI"
        p0?.foretemp.text = wt.main.temp.toString()+ "°C"
        Picasso.with(cntx).load("http://openweathermap.org/img/w/"+wt.weather[0].icon+".png").into(p0?.ficon)
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val foredate = itemView.findViewById(R.id.dateField) as TextView
        val forecondi = itemView.findViewById(R.id.condi3) as TextView
        val forepsi = itemView.findViewById(R.id.currentPSI3) as TextView
        val foretemp = itemView.findViewById(R.id.currenttempField2) as TextView
        val ficon = itemView.findViewById(R.id.futureCondiIcon) as ImageView
    }

}