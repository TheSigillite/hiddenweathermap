package com.example.weather5day

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import FiveDaysPrimary
import kotlinx.android.synthetic.main.activity_weather_display.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import WeatherNow
import android.app.Application
import android.content.SharedPreferences
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.*
import com.example.weather5day.Saveslots.App

import com.example.weather5day.Saveslots.Prefs
import list
import com.squareup.picasso.*
import org.slf4j.LoggerFactory

class WeatherDisplayActivity : AppCompatActivity() {
    lateinit var textdump : TextView
    var lat: Double = 20.0
    var lon: Double = 50.0
    var countrycode: String = ""
    var cityname: String = ""
    var located: Boolean = false
    var selected: Boolean = false
    val APPID: String = "f0f86ac9c889cc551630a3bfd1e973e4"
    lateinit var forecast: FiveDaysPrimary
    lateinit var weathercurrently: WeatherNow
    lateinit var recycledforecast: List<list>
    lateinit var weatherrecycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_display)
        weatherrecycler = findViewById(R.id.FutureWeather)
        weatherrecycler.layoutManager = LinearLayoutManager(this,LinearLayout.VERTICAL,false)
        retrieveExtras()
        downloadWeather()
        swipe_refresh.setOnRefreshListener {
            downloadWeather()
        }
        val savebutton = findViewById<Button>(R.id.SaveLocButton)
        savebutton.setOnClickListener{
            saveLocation()
        }

    }



    //
    //Getting Weather With GPS Coordiantes
    //
    fun getWeatherWithGPS(){
        val currenWeatherAPI = CurrenWeatherAPI.create()
        val ccast = currenWeatherAPI.getCurrentWeatherByCoordinates(lat,lon,APPID,"metric")
        ccast.enqueue(object: Callback<WeatherNow> {
            override fun onFailure(call: Call<WeatherNow>, t: Throwable) {
                var ftoast = Toast.makeText(applicationContext,t.toString(),Toast.LENGTH_SHORT)
                ftoast.show()
                val logger = LoggerFactory.getLogger(javaClass)
                logger.info(t.toString())
            }

            override fun onResponse(call: Call<WeatherNow>, response: Response<WeatherNow>) {
                if(response.code()==200){
                    weathercurrently = response.body()!!
                    //textdump.setText(forecast.toString())
                    bindCurrentWeather()

                }
                else{
                    var errtoast = Toast.makeText(applicationContext,response.code(),Toast.LENGTH_SHORT)
                    errtoast.show()
                }
            }
        })
        //
        //Gets 5 day 3h forecast
        //
        val fcast = currenWeatherAPI.getFiveDaysByCoords(lat,lon,APPID,"metric")
        fcast.enqueue(object: Callback<FiveDaysPrimary> {
            override fun onFailure(call: Call<FiveDaysPrimary>, t: Throwable) {
                var ftoast = Toast.makeText(applicationContext,t.toString(),Toast.LENGTH_SHORT)
                ftoast.show()
                val logger = LoggerFactory.getLogger(javaClass)
                logger.info(t.toString())
            }

            override fun onResponse(call: Call<FiveDaysPrimary>, response: Response<FiveDaysPrimary>) {
                if(response.code()==200){
                    forecast = response.body()!!
                    recycledforecast = forecast.list
                    val adapt = WeatherAdapter(recycledforecast)
                    weatherrecycler.adapter = adapt
                }
                else{
                    var errtoast = Toast.makeText(applicationContext,response.code(),Toast.LENGTH_SHORT)
                    errtoast.show()
                }
            }
        })

    }
    //
    //Getting Weather With Standrd Input
    //
    fun getWeatherWithLocation(){
        val locationWeatherAPI = CurrenWeatherAPI.create()
        val nowcast = locationWeatherAPI.getCurrentWeather(cityname+","+countrycode,APPID,"metric")
        nowcast.enqueue(object: Callback<WeatherNow> {
            override fun onFailure(call: Call<WeatherNow>, t: Throwable) {
                var ftoast = Toast.makeText(applicationContext,t.toString(),Toast.LENGTH_SHORT)
                ftoast.show()
                val logger = LoggerFactory.getLogger(javaClass)
                logger.info(t.toString())
            }

            override fun onResponse(call: Call<WeatherNow>, response: Response<WeatherNow>) {
                if(response.code()==200){
                    weathercurrently = response.body()!!
                    //textdump.setText(forecast.toString())
                    bindCurrentWeather()
                }
                else{
                    var errtoast = Toast.makeText(applicationContext,response.code(),Toast.LENGTH_SHORT)
                    errtoast.show()
                }
            }
        })
        //
        //Gets 5 days 3h forecast
        //
        val lcast = locationWeatherAPI.getFiveDaysForecast(cityname+","+countrycode,APPID,"metric")
        lcast.enqueue(object: Callback<FiveDaysPrimary> {
            override fun onFailure(call: Call<FiveDaysPrimary>, t: Throwable) {
                var ftoast = Toast.makeText(applicationContext,t.toString(),Toast.LENGTH_SHORT)
                ftoast.show()
                val logger = LoggerFactory.getLogger(javaClass)
                logger.info(t.toString())
            }

            override fun onResponse(call: Call<FiveDaysPrimary>, response: Response<FiveDaysPrimary>) {
                if(response.code()==200){
                    forecast = response.body()!!
                    recycledforecast = forecast.list
                    //textdump.setText(forecast.toString())
                    val adapt = WeatherAdapter(recycledforecast)
                    weatherrecycler.adapter = adapt
                }
                else{
                    var errtoast = Toast.makeText(applicationContext,response.code(),Toast.LENGTH_SHORT)
                    errtoast.show()
                }
            }
        })
    }

    fun bindCurrentWeather(){
        val locationcurrent = findViewById(R.id.locationCurrent) as TextView
        val condit = findViewById(R.id.condi) as TextView
        val PSInow = findViewById(R.id.currentPSI) as TextView
        val mintempe = findViewById(R.id.mintempField) as TextView
        val maxtempe = findViewById(R.id.maxtempField) as TextView
        val currenttempe = findViewById(R.id.currenttempField) as TextView
        val cimage = findViewById(R.id.currentWImage) as ImageView
        locationcurrent.text = getString(R.string.cty)+weathercurrently.name
        condit.text = getString(R.string.env)+weathercurrently.weather[0].main
        PSInow.text = getString(R.string.cpsi)+weathercurrently.main.pressure.toString()+" PSI"
        mintempe.text = getString(R.string.mtemp)+"\n"+weathercurrently.main.temp_min.toString()+"°C"
        maxtempe.text = getString(R.string.mxtemp)+"\n"+weathercurrently.main.temp_max.toString()+"°C"
        currenttempe.text = getString(R.string.rtemp)+"\n"+weathercurrently.main.temp.toString()+"°C"
        val imgurl: String = "http://openweathermap.org/img/w/"+weathercurrently.weather[0].icon+".png"
        Picasso.with(applicationContext).load(imgurl).into(cimage)
    }

    fun retrieveExtras(){
        located=intent.getBooleanExtra("located",false)
        selected=intent.getBooleanExtra("isSelected",false)
    }

    fun downloadWeather(){
        if(located){
            lat=intent.getDoubleExtra("lat",20.0)
            lon=intent.getDoubleExtra("lon",50.0)
            getWeatherWithGPS()
        } else if(selected){
            countrycode=intent.getStringExtra("countrycode")
            cityname=intent.getStringExtra("citynam")
            getWeatherWithLocation()
        }
    }

    private fun saveLocation() {
        val preferences = applicationContext.getSharedPreferences("lastSaveslot", 0)
        var lastSlot: Int = preferences.getInt("Slot", 1)
        if(lastSlot > 3){
            lastSlot = 1
        }
        val toast = Toast.makeText(
            applicationContext,
            "saved to slot " + lastSlot,
            Toast.LENGTH_SHORT
        )
        toast.show()
        if (lastSlot == 1) {
            if (located) {
                App.sav1!!.wasLocated = true
                App.sav1!!.savedLon = lon
                App.sav1!!.savedLat = lat
                App.sav1!!.savedCity = weathercurrently.name
            } else {
                App.sav1!!.wasLocated = false
                App.sav1!!.savedCity = cityname
                App.sav1!!.savedCode = countrycode
            }
        } else if (lastSlot == 2) {
            if (located) {
                App.sav2!!.wasLocated = true
                App.sav2!!.savedLon = lon
                App.sav2!!.savedLat = lat
                App.sav2!!.savedCity = weathercurrently.name
            } else {
                App.sav2!!.wasLocated = false
                App.sav2!!.savedCity = cityname
                App.sav2!!.savedCode = countrycode
            }
        } else if (lastSlot == 3) {
            if (located) {
                App.sav3!!.wasLocated = true
                App.sav3!!.savedLon = lon
                App.sav3!!.savedLat = lat
                App.sav3!!.savedCity = weathercurrently.name
            } else {
                App.sav3!!.wasLocated = false
                App.sav3!!.savedCity = cityname
                App.sav3!!.savedCode = countrycode
            }
        }
        lastSlot++
        preferences.edit().putInt("Slot", lastSlot).apply()
    }

}
