package com.example.weather5day

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import FiveDaysPrimary
import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.Gravity
import android.widget.ImageButton
import android.widget.Toast
import com.example.weather5day.Saveslots.App

import com.example.weather5day.Saveslots.Prefs
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import org.slf4j.LoggerFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    var fusedLocationClient: FusedLocationProviderClient? = null
    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //val co = App.cont!!.con
        retrieveAndBindSaves()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        val selectbutton = findViewById<ImageButton>(R.id.SelectImgButton)
        selectbutton.setOnClickListener{
            val intent = Intent(this,SelectorActivity::class.java)
            startActivity(intent)
        }
        val coordinatesbutton = findViewById<ImageButton>(R.id.LocateImgButton)
        coordinatesbutton.setOnClickListener{
            if(checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)) {
                this.fusedLocationClient?.lastLocation?.addOnSuccessListener(this) { location: Location? -> if (location==null){
                    var bruhtoast = Toast.makeText(applicationContext,"Could not get location. Try selecting your location",Toast.LENGTH_SHORT)
                    bruhtoast.show()
                }else{ location.apply { var oktoast = Toast.makeText(applicationContext,"lat: "+location.latitude+" lon: "+location.longitude,Toast.LENGTH_SHORT)
                    oktoast.show()
                    val locatedintent = Intent(applicationContext,WeatherDisplayActivity::class.java)
                    locatedintent.putExtra("lat",location.latitude)
                    locatedintent.putExtra("lon",location.longitude)
                    locatedintent.putExtra("located",true)
                    startActivity(locatedintent)}
                } }
            }
        }
    }

    val PERMISSION_ID = 42
    private fun checkPermission(vararg perm:String) : Boolean {
        val havePermissions = perm.toList().all {
            ContextCompat.checkSelfPermission(this,it) ==
                    PackageManager.PERMISSION_GRANTED
        }
        if (!havePermissions) {
            if(perm.toList().any {
                    ActivityCompat.
                        shouldShowRequestPermissionRationale(this, it)}
            ) {
                val dialog = AlertDialog.Builder(this)
                    .setTitle("Permission")
                    .setMessage("Permission needed!")
                    .setPositiveButton("OK", {id, v ->
                        ActivityCompat.requestPermissions(
                            this, perm, PERMISSION_ID)
                    })
                    .setNegativeButton("No", {id, v -> })
                    .create()
                dialog.show()
            } else {
                ActivityCompat.requestPermissions(this, perm, PERMISSION_ID)
            }
            return false
        }
        return true
    }

    fun retrieveAndBindSaves() {
        val slot1 = findViewById<Button>(R.id.Slot1)
        val slot2 = findViewById<Button>(R.id.Slot2)
        val slot3 = findViewById<Button>(R.id.Slot3)
        if(App.sav1!!.savedCity.isNotEmpty()){
            slot1.text = App.sav1!!.savedCity
            slot1.isEnabled = true
            slot1.setOnClickListener {
                val savedLocated = Intent(applicationContext,WeatherDisplayActivity::class.java)
                if (App.sav1!!.wasLocated){
                    savedLocated.putExtra("lat",App.sav1!!.savedLat)
                    savedLocated.putExtra("lon",App.sav1!!.savedLon)
                    savedLocated.putExtra("located",true)
                    startActivity(savedLocated)
                }else{
                    savedLocated.putExtra("countrycode",App.sav1!!.savedCode)
                    savedLocated.putExtra("citynam",App.sav1!!.savedCity)
                    savedLocated.putExtra("isSelected",true)
                    startActivity(savedLocated)
                }
            }
        }
        if(App.sav2!!.savedCity.isNotEmpty()){
            slot2.text = App.sav2!!.savedCity
            slot2.isEnabled = true
            slot2.setOnClickListener {
                val savedLocated = Intent(applicationContext,WeatherDisplayActivity::class.java)
                if (App.sav2!!.wasLocated){
                    savedLocated.putExtra("lat",App.sav2!!.savedLat)
                    savedLocated.putExtra("lon",App.sav2!!.savedLon)
                    savedLocated.putExtra("located",true)
                    startActivity(savedLocated)
                }else{
                    savedLocated.putExtra("countrycode",App.sav2!!.savedCode)
                    savedLocated.putExtra("citynam",App.sav2!!.savedCity)
                    savedLocated.putExtra("isSelected",true)
                    startActivity(savedLocated)
                }
            }
        }
        if(App.sav3!!.savedCity.isNotEmpty()){
            slot3.text = App.sav3!!.savedCity
            slot3.isEnabled = true
            slot3.setOnClickListener {
                val savedLocated = Intent(applicationContext,WeatherDisplayActivity::class.java)
                if (App.sav3!!.wasLocated){
                    savedLocated.putExtra("lat",App.sav3!!.savedLat)
                    savedLocated.putExtra("lon",App.sav3!!.savedLon)
                    savedLocated.putExtra("located",true)
                    startActivity(savedLocated)
                }else{
                    savedLocated.putExtra("countrycode",App.sav3!!.savedCode)
                    savedLocated.putExtra("citynam",App.sav3!!.savedCity)
                    savedLocated.putExtra("isSelected",true)
                    startActivity(savedLocated)
                }
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        this.recreate()
    }
}
