package com.example.weather5day.SpinnerModel

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.weather5day.R

import java.util.ArrayList

class CountryAdapter(context: Context, countryList: ArrayList<Country>) :
    ArrayAdapter<Country>(context, 0, countryList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    private fun initView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                R.layout.country_item, parent, false
            )
        }

        val imageViewFlag = convertView!!.findViewById<ImageView>(R.id.ctImage)
        val textViewName = convertView.findViewById<TextView>(R.id.ctText)

        val currentItem = getItem(position)

        if (currentItem != null) {
            imageViewFlag.setImageResource(currentItem.img)
            textViewName.text = currentItem.code
        }
        return convertView
    }
}
