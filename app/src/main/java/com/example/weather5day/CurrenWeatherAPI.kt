package com.example.weather5day

import retrofit2.Call
import retrofit2.http.GET
import FiveDaysPrimary
import WeatherNow
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Query

/*http://api.openweathermap.org/data/2.5/weather?q=Tarnow,pl&APPID=f0f86ac9c889cc551630a3bfd1e973e4&units=metric*/
//f0f86ac9c889cc551630a3bfd1e973e4
public interface CurrenWeatherAPI {
    @GET("/data/2.5/forecast")
    fun getFiveDaysForecast(@Query("q") q : String,@Query("APPID") APPID : String,@Query("units") units : String) : Call<FiveDaysPrimary>
    @GET("/data/2.5/forecast")
    fun getFiveDaysByCoords(@Query("lat") lat : Double,@Query("lon") lon : Double, @Query("APPID") APPID: String, @Query("units") units: String) : Call<FiveDaysPrimary>
    @GET("/data/2.5/weather")
    fun getCurrentWeather(@Query("q") q : String,@Query("APPID") APPID : String,@Query("units") units : String) : Call<WeatherNow>
    @GET("/data/2.5/weather")
    fun getCurrentWeatherByCoordinates(@Query("lat") lat : Double,@Query("lon") lon : Double, @Query("APPID") APPID: String, @Query("units") units: String) : Call<WeatherNow>
    companion object{
        fun create(): CurrenWeatherAPI{
            val retrofit = Retrofit.Builder().baseUrl("http://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create()).build()
            return retrofit.create(CurrenWeatherAPI::class.java)
        }
    }

}