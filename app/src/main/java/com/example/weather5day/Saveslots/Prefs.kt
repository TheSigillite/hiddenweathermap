package com.example.weather5day.Saveslots

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

class Prefs(context: Context,slot: Int) {
    val prefKey = "SaveSlot$slot"
    val cityKey = "city"
    val codeKey = "code"
    val locatedKey = "located"
    val lonKey = "lon"
    var latKey = "lat"
    val prefs: SharedPreferences = context.getSharedPreferences(prefKey,0)

    var savedCity: String
        get() = prefs.getString(cityKey,"")
        set(value) = prefs.edit().putString(cityKey,value).apply()

    var savedCode: String
        get() = prefs.getString(codeKey,"")
        set(value) = prefs.edit().putString(codeKey,value).apply()

    var savedLon: Double
        get() = prefs.getFloat(lonKey,20.0f).toDouble()
        set(value) = prefs.edit().putFloat(lonKey,value.toFloat()).apply()

    var savedLat: Double
        get() = prefs.getFloat(latKey,50.0f).toDouble()
        set(value) = prefs.edit().putFloat(latKey,value.toFloat()).apply()

    var wasLocated: Boolean
        get() = prefs.getBoolean(locatedKey,false)
        set(value) = prefs.edit().putBoolean(locatedKey,value).apply()
}


class App: Application(){
    companion object{
        var sav1: Prefs? = null
        var sav2: Prefs? = null
        var sav3: Prefs? = null

    }

    override fun onCreate() {

        sav1 = Prefs(applicationContext,1)
        sav2 = Prefs(applicationContext,2)
        sav3 = Prefs(applicationContext,3)
        super.onCreate()
    }
}
